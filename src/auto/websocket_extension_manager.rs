// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use crate::{ffi, SessionFeature};

glib::wrapper! {
    #[doc(alias = "SoupWebsocketExtensionManager")]
    pub struct WebsocketExtensionManager(Object<ffi::SoupWebsocketExtensionManager, ffi::SoupWebsocketExtensionManagerClass>) @implements SessionFeature;

    match fn {
        type_ => || ffi::soup_websocket_extension_manager_get_type(),
    }
}

impl WebsocketExtensionManager {}
